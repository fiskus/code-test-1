Array.prototype.groupBy = function (groupFunc) {
  if (!Array.isArray(this)) {
    throw new Error('`this` is not Array');
  }
  return this.reduce(function (prev, item) {
    var key = typeof groupFunc === 'function' ?
        groupFunc(item) : item;
    if (prev[key]) {
      prev[key].push(item);
    } else {
      prev[key] = [item];
    }
    return prev;
  }, {});
};
