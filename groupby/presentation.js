var Ids = {
  INPUT: 'input',
  OUTPUT: 'output',
  SUBMIT_DIV_3: 'submit-div-3',
  SUBMIT_GENERIC: 'submit-generic'
};

var outputEl = document.getElementById(Ids.OUTPUT);


function parse () {
  var input = document.getElementById(Ids.INPUT);
  return JSON.parse(input.value);
}

function print (value) {
  outputEl.textContent = JSON.stringify(value);
}

function onSubmitGeneric () {
  var list = parse();
  var output = list.groupBy();
  print(output);
}

function onSubmitDivBy3 () {
  var list = parse();
  var output = list.groupBy(function (value) {
    return value % 3;
  });
  print(output);
}

function main () {
  var submitDiv3El = document.getElementById(Ids.SUBMIT_DIV_3);
  submitDiv3El.addEventListener('click', onSubmitDivBy3);
  var submitGeneric = document.getElementById(Ids.SUBMIT_GENERIC);
  submitGeneric.addEventListener('click', onSubmitGeneric);
}

main();
