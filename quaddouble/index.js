function quaddouble (num1, num2) {
  var quadStr = num1.toString();
  var doubleStr = num2.toString();
  if (!quadStr || !doubleStr) {
    throw new Error('Wrong input data');
  }

  var quads = findQuads(quadStr);
  return hasDoubleMathes(doubleStr, quads);
}

function findQuads (quadStr) {
  function getLastQuad () {
    return quads[quads.length - 1];
  }

  function setLastQuad (quad) {
    return quads[quads.length - 1] = quad;
  }

  var quads = [''];

  [].slice.apply(quadStr).forEach(function (letter, index, arr) {
    var quad = getLastQuad();
    if (quad.indexOf(letter) >= 0) {
      quad += letter;
    } else {
      quad = letter;
    }
    setLastQuad(quad);

    if (quad.length === 4) {
      quads.push('');
    }

    if (index === arr.length - 1) {
      quads.pop();
    }
  }, '');

  return quads;
}

function hasDoubleMathes (doubleStr, quads) {
  return quads.some(function(quad) {
    var letter = quad[0];
    return doubleStr.indexOf(letter + letter) >= 0;
  });
}
