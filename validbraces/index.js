var noBraces = '[^\\]\\[\\(\\){}]*';

function validBraces (braces) {
  var collapsePairs = removeStrictlyMatchedPairs(braces);

  if (collapsePairs.length) {
    console.log(collapsePairs, braces);
    if (collapsePairs.length === braces.length) {
      return false;
    } else {
      return validBraces(collapsePairs);
    }
  } else {
    return true;
  }
}

function removeStrictlyMatchedPairs (braces) {
  var roundBracesRegexp = new RegExp('\\(' + noBraces + '\\)', 'g');
  var roundBracesCollapsed = braces.replace(roundBracesRegexp, '');

  var squareBracesRegexp = new RegExp('\\[' + noBraces + ']', 'g');
  var squareBracesCollapsed = roundBracesCollapsed.replace(squareBracesRegexp, '');

  var curlyBracesRegexp = new RegExp('{' + noBraces + '}', 'g');
  var curlyBracesCollapsed = squareBracesCollapsed.replace(curlyBracesRegexp, '');
  return curlyBracesCollapsed;
}
