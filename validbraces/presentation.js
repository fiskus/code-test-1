var Ids = {
  INPUT: 'input',
  OUTPUT: 'output',
  SUBMIT: 'submit'
};

var outputEl = document.getElementById(Ids.OUTPUT);


function parse () {
  var input = document.getElementById(Ids.INPUT);
  return input.value;
}

function print (value) {
  outputEl.textContent = value;
}

function onSubmit () {
  var bracedText = parse();
  var output = validBraces(bracedText);
  print(output);
}

function main () {
  var submit = document.getElementById(Ids.SUBMIT);
  submit.addEventListener('click', onSubmit);
}

main();
